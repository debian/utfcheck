#!/bin/sh

set -e

# The input file to convert
INFILE=./samples/sample-utf8.txt.gz

# The output file of the conversion
OUTFILE=test-utf8-expurgated.txt

# The reference file to compare the output against
CMPFILE=./expected/out-utf8-expurgated.txt

#
# Create temporary directory for test
# output if AUTOPKGTEST_TMP is undefined.
# Debian GNU/Linux defines AUTOPKGTEST_TMP.
#
if [ "x${AUTOPKGTEST_TMP}" = "x" ] ; then
   TEST_TMP=$(mktemp -d)
   trap "\rm -rf ${AUTOPKGTEST_TMP}" 0 INT QUIT ABRT PIPE TERM
else
   TEST_TMP=${AUTOPKGTEST_TMP}
fi

#
# Point to the source directory for test.
#
if [ "x${srcdir}" = "x" ] ; then
   srcdir=.
fi

#
# Point to utf8 executable; utfcheck_bindir
# should be defined for "make installcheck".
# Otherwise, leave undefined for "make check".
#
if [ "x${utfcheck_bindir}" = "x" ] ; then
   utfcheck_bindir=../src
fi

#
# Ignore error exit status so we can keep going and
# compare the utfcheck output with the expected output.
#
( gunzip < ${srcdir}/${INFILE} | \
   ${utfcheck_bindir}/utfcheck --expurgated > ${TEST_TMP}/${OUTFILE} ) || true

diff ${srcdir}/${CMPFILE} ${TEST_TMP}/${OUTFILE} || \
	(echo "test-utf8-expurgated FAILED; output in ${TEST_TMP}/${OUTFILE}" ; exit 1)

#
# If AUTOPKGTEST_TMP was defined, don't remove it;
# a Debian calling process will take care of that.
#
if [ "x${AUTOPKGTEST_TMP}" = "x" ] ; then
   \rm -rf ${TEST_TMP}
fi

